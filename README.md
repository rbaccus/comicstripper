# ComicStripper

ComicStripper is a MEAN stack web app that copies content from a web site to the local HTML page to help make reading Comics and other content faster without all of the ads and popups.

ComicStripper takes an URL and JQuery values, then downloads the HTML from the URL, searches the HTML for the values/content based on the JQuery, and copies the content to the Local HTML page.

This is architected into 3 main container images that can be deployed to Kubernetes, or other container orchastrator.  Using the Model, View, Controller (MVC) design pattern, each will be a container image that will be the first Cloud Application tutorial and example used in [IcePanel](http://icepanel.io/).

# About the Author
Rob Baccus is Founder and CEO of [Indigon Inc.](http://indigoninc.com/) which produces various applications and consults on a variarity of projects.  Rob is also the Service Architect and Tech Lead for the Boeing Kubernetes Container Service in Boeing Enterprise Cloud Services group.  His passion is to provide customers with innovative solutions to their problems and/or provide new innovative applications to help automate customer's daily routines.